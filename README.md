#  cryo_agent_driver

QCoDeS instrument driver for connecting to a cryo-agent running on a FridgePC. 

## Installation 

Using pip:
```sh
pip install cryo-agent-driver --extra-index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```


## Usage 

```python
from cryo_agent_driver.instrument import CryoAgentInstrument

# Create CryoAgentInstrument
cryo_agent_instr = CryoAgentInstrument(
    name="test_fridge", 
    url="http://localhost:8081",
    pv_map = {
        "temperature": "50K-flange",
    },
    max_val_age = 60 #seconds
)

# Retrieve data from it
res = cryo_agent_instr.data() 

# Print the results
print(res)

# Retrieve data for specific parameter
temp = cryo_agent_instr.temperature()

# Print the temperature
print(temp)
```