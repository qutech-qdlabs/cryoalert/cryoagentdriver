from cryo_agent_driver.instrument import CryoAgentInstrument


def test_instrument_construction():
    """Simple test to check if the constructor is successfull"""
    cai = CryoAgentInstrument(
        name="CryoAgentInstrument_test",
        url="http://localhost:8081",
        pv_map={
            "temperature": "50K-flange",
        },
    )

    res1 = cai.data()
    print(res1)

    res2 = cai.temperature()
    print(res2)
    assert res2.var_name == "50K-flange"
