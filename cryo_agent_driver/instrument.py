import logging
from functools import partial
from typing import Any, Dict

from cryo_agent_api_client.api import CryoAgentApiClient
from cryo_agent_api_client.models import CryoAgentDataPoint
from qcodes.instrument.base import Instrument

LOGGER = logging.getLogger(__name__)


class CryoAgentInstrument(Instrument):
    """
    A QCoDeS instrument that implements the cryo-agent api client
    to retrieve data for a fridge.
    """

    def __init__(self, name: str, url: str, pv_map: Dict[str, str], **kwargs) -> None:
        """
        Constructor for the CryoAgentInstrument.

        Args:
            name (str): an identifier for this instrument, particularly for attaching it to a Station.
            url (str): url of the cryo-agent attached to the fridge
            pv_map (Dict[str, str]): a map describing the mapping from instrument parameters to corresponding variable
                names coming from the Fridge

        Kwargs:
            max_val_age (int): Maximum valid time for which the data retrieved from the cryo-agent is cached.
                default is 60 seconds.

        """
        max_val_age = kwargs.pop("max_val_age", 60)
        super().__init__(name, **kwargs)

        # store url of the CryoAgent
        self._agent_url = url

        # Add the single parameter
        self.add_parameter(
            name="data",
            label=f"data of the fridge with an agent located at url={self._agent_url}",
            get_cmd=self._get_data,
            set_cmd=False,
            max_val_age=max_val_age,
        )

        # Using the paramater-variable name mapping, add QCoDeS parameters to the instrument
        for par_name, var_name in pv_map.items():
            self.add_parameter(
                name=par_name,
                label=f"data of the fridge with parameter name {par_name} mapped to variable name {var_name}",
                get_cmd=partial(self._get_var, var_name),
                set_cmd=False,
            )

    def _get_data(self) -> Dict[str, CryoAgentDataPoint]:
        """
        Getter command for the Fridge data as provided by a cryo_agent.
        If no connection could be made to the cryo_agent it return None instead.

        Returns:
            Dict[str, CryoAgentDataPoint]: sorted dictionary of all datapoints returned from the cryo-agent.

        """
        with CryoAgentApiClient(url=self._agent_url) as client:
            raw_data = client.get_latest_data()

        # Check if raw_data is not empty
        if raw_data is None:
            return {}

        # Sort for easier access later
        return {dp.var_name: dp for dp in raw_data.data.data}

    def get_raw_data(self) -> Dict[str, CryoAgentDataPoint]:
        """
        Method to get raw data, potentially cached.

        Returns:
            Dict[str, CryoAgentDataPoint]: sorted dictionary of all datapoints returned from the cryo-agent.

        """
        return self.data.cache()

    def _get_var(self, var_name: str) -> Any:
        # TODO, probably add some checks etc.
        return self.get_raw_data().get(var_name)
